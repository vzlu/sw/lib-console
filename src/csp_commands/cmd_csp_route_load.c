//
// Load routing table from file
//

#include "console/cmddef.h"
#include "x_csp_rt.h"
#include <errno.h>

/**
 * Default implementation of `cmd_csp_route_load_hook`
 * - can be called from user code overriding the weak stub
 */
console_err_t __attribute__((weak)) cmd_csp_route_load_hook_default(char *buffer, size_t capacity, const char *fname) {
    FILE *f = fopen(fname, "r");
    if (f == NULL) {
        console_printf("Error opening file: %s", strerror(errno));
        return CONSOLE_ERR_IO;
    } else {
        // find length
        fseek(f, 0L, SEEK_END);
        size_t numbytes = ftell(f);
        // rewind
        fseek(f, 0L, SEEK_SET);

        if (numbytes >= capacity) {
            console_printf("File too large: %zu bytes\n", numbytes);
            fclose(f);
            return CONSOLE_ERR_IO;
        } else {
            fread(buffer, 1, numbytes, f);
            buffer[numbytes] = 0; // ensure it's terminated
        }
        fclose(f);
    }

    return CONSOLE_OK;
}

/**
 * Load routes from a file.
 *
 * This default implementation has weak linkage to allow overriding.
 *
 * @param[in] exported - the route table string exported from CSP
 * @param[in] fname - specified file name
 */
console_err_t __attribute__((weak)) cmd_csp_route_load_hook(char *buffer, size_t capacity, const char *fname) {
    return cmd_csp_route_load_hook_default(buffer, capacity, fname);
}

int cmd_csp_route_load(console_ctx_t *ctx, cmd_signature_t *reg)
{
    (void) ctx; // unused
    static struct {
        struct arg_file *file;
        struct arg_end *end;
    } args;

    if (reg) {
        args.file = arg_file0(NULL, NULL, "<file>", EXPENDABLE_STRING("source file, defaults to \"routes.txt\""));
        args.end = arg_end(1);

        reg->argtable = &args;
        reg->help = EXPENDABLE_STRING("Load the routing table from a file");
        return 0;
    }

    const char *fname = args.file->count ?
                        args.file->filename[0] : "routes.txt";

    console_printf("Loading routing table from file: %s\n", fname);

    console_err_t rv = 1; // generic error
    if (CONSOLE_OK == (rv = cmd_csp_route_load_hook(x_rtable_buffer, x_RTABLE_BUFLEN, fname))) {
        console_print("Loading: ");
        console_write(x_rtable_buffer, -1);
        console_print("\n");

        int count = csp_rtable_load(x_rtable_buffer);
        if (count >= 0) {
            // Print the real final state in libcsp
            x_csp_read_and_print_routes();
            return CONSOLE_OK;
        }
        return count;
    }
    return rv;
}
