//
// Show local CSP routes
//

#include "console/cmddef.h"
#include "x_csp_rt.h"

int cmd_csp_route_list(console_ctx_t *ctx, cmd_signature_t *reg)
{
    (void) ctx; // unused
    if (reg) {
        reg->help = EXPENDABLE_STRING("Show local CSP routing table");
        return 0;
    }

    x_csp_rtable_t * rtable = x_csp_rtable_import_from_libcsp();
    console_print_routes(rtable);
    x_csp_rtable_clear(&rtable);

    return 0;
}
