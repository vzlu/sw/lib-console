//
// Created by MightyPork on 2018/12/08.
//

#include "console/cmddef.h"
#include <csp/csp_cmp.h>
#include <csp/csp_endian.h>


int cmd_csp_poke(console_ctx_t *ctx, cmd_signature_t *reg)
{
    (void) ctx; // unused
    static struct {
        struct arg_int *node;
        struct arg_int *timeout;
        struct arg_str *addr;
        struct arg_str *bytes;
        struct arg_end *end;
    } args;

    if (reg) {
        args.node = arg_cspaddr1();
        args.timeout = arg_timeout0();
        args.addr = arg_str1(NULL, NULL, "<addr>", EXPENDABLE_STRING("address (hex without 0x)"));
        args.bytes = arg_str1(NULL, NULL, "<bytes>", EXPENDABLE_STRING("base16 bytes"));
        args.end = arg_end(3);

        reg->argtable = &args;
        reg->help = EXPENDABLE_STRING("Poke CSP node memory via CMP");
        return 0;
    }

    int node = args.node->ival[0];
    int timeout = GET_ARG_TIMEOUT0(args.timeout);

    unsigned int addr;
    unsigned char data[CSP_CMP_POKE_MAX_LEN];

    if (sscanf(args.addr->sval[0], "%x", &addr) != 1)
        return 1;

    int len = console_base16_decode(args.bytes->sval[0], data, CSP_CMP_POKE_MAX_LEN);
    if (len < 0) {
        console_printf("Hex format error!\n");
        return 1;
    }

    console_printf("Writing to mem at node %u addr 0x%x len %u timeout %d\n", node, addr, len, timeout);

    console_hexdump(data, len);

    struct csp_cmp_message msg;
    msg.poke.addr = csp_hton32(addr);
    msg.poke.len = len;
    memcpy(msg.poke.data, data, CSP_CMP_POKE_MAX_LEN);

    int ret = csp_cmp_poke(node, timeout, &msg);
    if (ret != CSP_ERR_NONE) {
        console_printf("Error: %d\n", ret);
        return 1;
    }

    console_printf("Done\n");

    return 0;
}
