//
// Identify a remote CSP node
//

#include "console/cmddef.h"
#include <csp/csp_cmp.h>

int cmd_csp_ident(console_ctx_t *ctx, cmd_signature_t *reg)
{
    (void) ctx; // unused
    static struct {
        struct arg_int *node;
        struct arg_int *timeout;
        struct arg_end *end;
    } args;

    if (reg) {
        args.node = arg_cspaddr0();
        args.timeout = arg_timeout0();
        args.end = arg_end(2);

        reg->argtable = &args;
        reg->help = EXPENDABLE_STRING("Identify a remote CSP node via CMP");
        return 0;
    }

    int timeout = GET_ARG_TIMEOUT0(args.timeout);
    int node = GET_ARG_CSPADDR0(args.node);

    struct csp_cmp_message msg;
    int ret = csp_cmp_ident(node, timeout, &msg);
    if (ret != CSP_ERR_NONE) {
        console_printf("error: %d\n", ret);
        return ret;
    }

    console_printf("Hostname: %s\n", msg.ident.hostname);
    console_printf("Model:    %s\n", msg.ident.model);
    console_printf("Revision: %s\n", msg.ident.revision);
    console_printf("Date:     %s\n", msg.ident.date);
    console_printf("Time:     %s\n", msg.ident.time);

    return 0;
}
