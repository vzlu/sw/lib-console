//
// Stress-testing command
//

//#define LOG_LOCAL_LEVEL ESP_LOG_DEBUG

#include <esp_log.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <console/console_main.h>
#include <csp/csp.h>
#include <csp/arch/csp_time.h>

#include "cmd_stress_flood.h"
#include "console/cmddef.h"
#include "csp/csp_conn.h"
#include "common_utils/utils.h"

static const char* TAG = "flood";

static struct {
    struct arg_int *node;
    struct arg_int *timeout;
    struct arg_int *interval;
    struct arg_int *size;
    struct arg_lit *opt_r;
    struct arg_lit *opt_x;
    struct arg_lit *opt_h;
    struct arg_lit *opt_c;
    struct arg_end *end;
} args;

struct test_options {
    uint32_t options;
    int timeout;
    int interval;
    int size;
    int node;
};

static void run_flood_stress(const struct test_options *opts);

#define DEF_INTERVAL 0
#define SFL_PACKET_OVERHEAD 5
#define SFL_DEF_SIZE 16

/** 'stress' command stress-tests an interface */
static int cmd_stress_flood(int argc, char **argv)
{
    CMD_CHECK_ARGS(args);

    const int node = args.node->ival[0];
    const int timeout = (args.timeout->count) ? args.timeout->ival[0] : DEF_CSP_TIMEOUT;
    const int interval = (args.interval->count) ? args.interval->ival[0] : DEF_INTERVAL;

    uint32_t options = CSP_O_NONE;
    if (args.opt_r->count) options |= CSP_O_RDP;
    if (args.opt_x->count) options |= CSP_O_XTEA;
    if (args.opt_h->count) options |= CSP_O_HMAC;
    if (args.opt_c->count) options |= CSP_O_CRC32;

    // --- choose mode ---

    const int size = (args.size->count) ? args.size->ival[0] : SFL_DEF_SIZE;

    const struct test_options opts = {
        .node = node,
        .options = options,
        .size = size,
        .timeout = timeout,
        .interval = interval,
    };
    run_flood_stress(&opts);

    return 0;
}

enum s1w_result {
    SFL_SUCCESS = 0,
    SFL_FAIL = 1,
};


static enum s1w_result op1way_establish(const struct test_options *opts, csp_conn_t *conn)
{
    enum s1w_result status = SFL_FAIL;

    // !!! No logging should occur here, except for error messages on failure!

    /* Prepare data */
    csp_packet_t * packet = csp_buffer_get(sizeof(struct sfl_establish));
    if (packet == NULL) {
        ESP_LOGE(TAG, "Packet alloc failed");
        goto cleanup;
    }

    packet->length = sizeof(struct sfl_establish);
    struct sfl_establish *pdata = (void*) packet->data;
    pdata->opcode = BENCH_ESTABLISH_1;
    pdata->payload_size = opts->size;
    pdata->timestamp = csp_get_ms();

    if (!csp_send(conn, packet, 0)) {
        ESP_LOGE(TAG, "Send failed (E1)");
        goto cleanup;
    }

    /* Read incoming frame 2 */
    packet = csp_read(conn, opts->timeout);
    if (packet == NULL) {
        ESP_LOGE(TAG, "Read timed out (E2)");
        goto cleanup;
    }

    if (packet->data[0] != BENCH_ESTABLISH_2) {
        ESP_LOGE(TAG, "Received unexpected opcode 0x%02x (E2)", packet->data[0]);
        goto cleanup;
    }

    // let's recycle the received frame
    pdata = (void*) packet->data;
    pdata->opcode = BENCH_ESTABLISH_3;
    pdata->payload_size = 0;
    pdata->timestamp = csp_get_ms();

    if (!csp_send(conn, packet, 0)) {
        console_printf("Send failed (E3)");
        goto cleanup;
    }

    /* Read incoming frame (ESTABLISH_4) */
    packet = csp_read(conn, opts->timeout);
    if (packet == NULL) {
        ESP_LOGE(TAG, "Read timed out (E4)");
        goto cleanup;
    }

    if (packet->data[0] != BENCH_ESTABLISH_4) {
        ESP_LOGE(TAG, "Received unexpected opcode 0x%02x (E4)", packet->data[0]);
        goto cleanup;
    }

    status = SFL_SUCCESS;

cleanup:
    /* Clean up */
    if (packet != NULL) {
        csp_buffer_free(packet); // free the received packet
    }

    return status;
}

static enum s1w_result op1way_packet(csp_conn_t *conn, const struct test_options *opts)
{
    static uint8_t seed = 0;

    csp_packet_t * packet = csp_buffer_get(opts->size);
    if (packet == NULL) {
        ESP_LOGE(TAG, "Packet alloc failed");
        return SFL_FAIL;
    }

    packet->length = opts->size;

    uint32_t payload_len = opts->size - sizeof(struct sfl_packet);
    //ESP_LOGD(TAG, "payload len to fill with numbers is %d", payload_len);//FIXME remove

    struct sfl_packet *pdata = (void*)packet->data;
    pdata->opcode = BENCH_PACKET;
    pdata->timestamp = csp_get_ms();

    // fill it with growing bytes ping-style
    uint8_t counter = seed++;
    for (uint8_t i = 0; i < payload_len; i++, counter++) {
        pdata->payload[i] = counter;
    }

    /* Try to send frame */
    if (!csp_send(conn, packet, 0)) {
        ESP_LOGE(TAG, "Send failed");
        return SFL_FAIL;
    }

    return SFL_SUCCESS;
}

static enum s1w_result op1way_stop(csp_conn_t *conn, const struct test_options *opts, struct sfl_report *stats, uint32_t sent_count)
{
    enum s1w_result status = SFL_FAIL;

    /* Prepare data */
    csp_packet_t * packet = csp_buffer_get(sizeof(struct sfl_stop));
    if (packet == NULL) {
        ESP_LOGE(TAG, "Packet alloc failed");
        return SFL_FAIL;
    }

    /* Set data to increasing numbers */
    packet->length = sizeof(struct sfl_stop);

    struct sfl_stop *pdata = (void*) packet->data;
    pdata->opcode = BENCH_REQUEST_REPORT;
    pdata->count_sent = sent_count;

    /* Try to send frame */
    if (!csp_send(conn, packet, 0)) {
        ESP_LOGE(TAG, "Send failed");
        goto cleanup;
    }

    /* Read incoming frame */
    packet = csp_read(conn, opts->timeout);
    if (packet == NULL) {
        ESP_LOGE(TAG, "Read timed out");
        goto cleanup;
    }

    if (packet->data[0] == BENCH_REPORT) {
        status = SFL_SUCCESS;
        memcpy(stats, packet->data, sizeof(struct sfl_report));
    }

cleanup:
    /* Clean up */
    if (packet != NULL)
        csp_buffer_free(packet); // free the received packet

    return status;
}

static void print_test_results(struct sfl_report *stats, uint32_t sent_count, uint32_t apiece, uint32_t elapsed_ms);

static void run_flood_stress(const struct test_options *opts)
{
    if (opts->size < SFL_PACKET_OVERHEAD) {
        console_color_printf(COLOR_RED, "Payload must have at least %d bytes for 1-way test!\r\n", SFL_PACKET_OVERHEAD);
        return;
    }

    console_printf("Starting 1-way test (target: %d, len: %d, ival: %d ms, tout: %d ms).\r\n"
                   "Press any key to stop.\r\n",
                   opts->node, opts->size, opts->interval, opts->timeout);
    console_fputs("Establishing test session...\r\n");

    csp_conn_t *conn = csp_connect(CSP_PRIO_NORM, opts->node, BENCHMARK_PORT, opts->timeout, opts->options);
    if (conn == NULL) {
        ESP_LOGE(TAG, "Connect failed");
        return;
    }

    if (SFL_SUCCESS != op1way_establish(opts, conn)) {
        console_color_printf(COLOR_RED, "Failed to establish test session!\r\n");
        return;
    }

    console_fputs("Connected, starting 1-way test...\r\n");

    assert(conn != NULL);

    char inkey;
    enum s1w_result suc;
    uint32_t sent_count = 0;
    // the session is now established, let's spam packets!
    const uint32_t start_time = csp_get_ms();
    while (1) {
        suc = op1way_packet(conn, opts);
        if (suc != SFL_SUCCESS) {
            // failed to alloc or send?
            console_color_printf(COLOR_RED, "Error sending packet, abort test\r\n");
            break;
        }
        sent_count++;

        if (opts->interval > 0) {
            vTaskDelay(pdMS_TO_TICKS(opts->interval));
        }

        if (0 != console_try_read(&inkey, 1) && inkey != 0) {
            break;
        }
    }
    const uint32_t end_time = csp_get_ms();
    console_fputs("Test ended, requesting report from remote...\r\n");
    struct sfl_report stats = {};
    suc = op1way_stop(conn, opts, &stats, sent_count);
    if (suc == SFL_SUCCESS) {
        print_test_results(&stats, sent_count, opts->size, end_time - start_time);
    } else {
        console_color_printf(COLOR_RED, "Failed to get test report\r\n");
    }

    csp_close(conn);
}

static void print_test_results(struct sfl_report *stats, uint32_t sent_count, uint32_t apiece, uint32_t elapsed_ms)
{
    const uint64_t raw_bytes = apiece * stats->count_received;
    const float byterate = ((float)raw_bytes / elapsed_ms) * 1000.0f;

    console_printf("Test ended in %.2f seconds\r\n"
                   "--- Stats ---\r\n"
                   "Packets sent: %d\r\n"
                   "Packets received: %d (loss %d / %.2f%%)\r\n"
                   "Latency: %d - %d ms\r\n"
                   "Data rate: %.1f B/s\r\n",
                   elapsed_ms / 1000.0f,
                   sent_count,
                   stats->count_received,
                   sent_count - stats->count_received,
                   sent_count == 0 ? 0.0f : 100.0f * ((sent_count - stats->count_received) / (float) sent_count),
                   stats->min_latency,
                   stats->max_latency,
                   byterate);
}

struct stress1w_state {
    uint32_t t1;
    uint32_t t1b;
    uint32_t t2;
    uint32_t t2b;
    uint32_t start_time;
    uint32_t expected_payload_size;
    int32_t local_time_offset;
    int32_t latency_min;
    int32_t latency_max;
    uint32_t packet_count;
};

static struct stress1w_state g_s1w_slave_state = {};

void stress_flood_handle_slave(csp_conn_t *conn, csp_packet_t *packet)
{
    uint32_t time = csp_get_ms();

    struct sfl_establish *pestablish;
    struct sfl_packet *ppacket;
    struct sfl_report *preport;
    struct sfl_stop *prequest;

//    ESP_LOGD(TAG, "Rx benchmark packet: 0x%02x", packet->data[0]);//TODO remove

    switch (packet->data[0]) {
        case BENCH_ESTABLISH_1:
            pestablish = (void *) packet->data;
            g_s1w_slave_state.t1 = pestablish->timestamp;
            g_s1w_slave_state.t1b = time;
            g_s1w_slave_state.expected_payload_size = pestablish->payload_size;

            pestablish->opcode = BENCH_ESTABLISH_2;
            g_s1w_slave_state.t2 = pestablish->timestamp = csp_get_ms();
            if (!csp_send(conn, packet, DEF_CSP_TIMEOUT)) {
                csp_buffer_free(packet);
            }
            break;

        case BENCH_ESTABLISH_3:
            pestablish = (void *) packet->data;
            g_s1w_slave_state.t2b = pestablish->timestamp;

            g_s1w_slave_state.local_time_offset =
                ((int32_t)g_s1w_slave_state.t1b - (int32_t)g_s1w_slave_state.t1 - (int32_t)g_s1w_slave_state.t2b + (int32_t)g_s1w_slave_state.t2) / 2;

            console_printf("1-way test session established, local time offset is: %d\r\n", g_s1w_slave_state.local_time_offset);

            g_s1w_slave_state.latency_max = INT32_MIN;
            g_s1w_slave_state.latency_min = INT32_MAX;
            g_s1w_slave_state.packet_count = 0;
            g_s1w_slave_state.start_time = time;

            pestablish->opcode = BENCH_ESTABLISH_4;
            if (!csp_send(conn, packet, DEF_CSP_TIMEOUT)) {
                csp_buffer_free(packet);
            }
            break;

        case BENCH_PACKET:
            ppacket = (void*) packet->data;
            int32_t send_time = ppacket->timestamp + g_s1w_slave_state.local_time_offset;
            bool ok = true;

            if (packet->length != g_s1w_slave_state.expected_payload_size) {
                ESP_LOGW(TAG, "Wrong len %d (expected %d)",
                    packet->length, g_s1w_slave_state.expected_payload_size);
            }
            else {
                uint8_t counter = ppacket->payload[0];
                for (int i = 0; i < g_s1w_slave_state.expected_payload_size - SFL_PACKET_OVERHEAD; i++, counter++) {
                    if (ppacket->payload[i] != counter) {
                        ok = false;
                        ESP_LOGW(TAG, "Malformed packet");
                        ESP_LOG_BUFFER_HEX(TAG, ppacket->payload,
                            g_s1w_slave_state.expected_payload_size - SFL_PACKET_OVERHEAD);
                        break;
                    }
                }

                if (ok) {
                    g_s1w_slave_state.packet_count++;

                    int32_t latency = time - send_time;
                    if (latency < g_s1w_slave_state.latency_min) {
                        g_s1w_slave_state.latency_min = latency;
                    } else if (latency > g_s1w_slave_state.latency_max) {
                        g_s1w_slave_state.latency_max = latency;
                    }

//                    ESP_LOGD(TAG, "Send time %d (raw %d), local time %d", send_time, ppacket->timestamp, time);//TODO remove
//                    ESP_LOGD(TAG, "Latency %d ms", latency);//TODO remove
                }
            }
            csp_buffer_free(packet);
            break;

        case BENCH_REQUEST_REPORT:
            prequest = (void *) packet->data;
            uint32_t sent_count = prequest->count_sent;

            // request report
            preport = (void*) packet->data;
            preport->opcode = BENCH_REPORT;
            preport->min_latency = g_s1w_slave_state.latency_min;
            preport->max_latency = g_s1w_slave_state.latency_max;
            preport->count_received = g_s1w_slave_state.packet_count;

            // print stats on the slave end too
            print_test_results(preport, sent_count, g_s1w_slave_state.expected_payload_size, time - g_s1w_slave_state.start_time);

            // send the report
            if (!csp_send(conn, packet, DEF_CSP_TIMEOUT)) {
                csp_buffer_free(packet);
            }
            break;
    }
}

void register_cmd_stress_flood(void)
{
    args.node = arg_int1(NULL, NULL, "<node>", EXPENDABLE_STRING("node ID"));
    args.timeout = arg_int0("t", "timeout", "<ms>", EXPENDABLE_STRING("timeout in ms (ping & 1way test handshakes)"));
    args.interval = arg_int0("n", "period", "<ms>", EXPENDABLE_STRING("period in ms (for 1way test). Default 1ms"));
    args.size = arg_int0("s", "size", "<bytes>", EXPENDABLE_STRING("payload size (default ")STR(SFL_DEF_SIZE)")");
    args.opt_r = arg_lit0("r", NULL, EXPENDABLE_STRING("use RDP"));
    args.opt_x = arg_lit0("x", NULL, EXPENDABLE_STRING("use XTEA"));
    args.opt_h = arg_lit0("H", NULL, EXPENDABLE_STRING("use HMAC"));
    args.opt_c = arg_lit0("c", NULL, EXPENDABLE_STRING("use CRC32"));
    args.end = arg_end(10);

    const esp_console_cmd_t cmd = {
        .command = "flood",
        .help = "Measure CSP connection one-way latencies and packet loss",
        .hint = NULL,
        .func = &cmd_stress_flood,
        .argtable = &args
    };
    ESP_ERROR_CHECK( esp_console_cmd_register(&cmd) );
}
