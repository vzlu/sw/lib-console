/**
 * 1-way stress global symbols
 *
 * this is the `flood` command from csp-emu, requires special handling in CSP receive loop
 * 
 * Created on 2019/05/19.
 */

#ifndef CSPEMU_CMD_STRESS_FLOOD_H
#define CSPEMU_CMD_STRESS_FLOOD_H

#include <stdint.h>

#define BENCHMARK_PORT 7

enum csp_bench_opcode {
    BENCH_OPCODE_MIN = 0,
    // PTP-inspired synchronization for 1-way latency measurement
    // 1) Current master time -> slave
    BENCH_ESTABLISH_1 = 0,
    // 2) Dummy packet slave -> master
    BENCH_ESTABLISH_2 = 1,
    // 3) Master reception time of ESTABLISH_2 -> slave
    BENCH_ESTABLISH_3 = 2,
    // 4) Confirmation from slave that we can start sending packets
    BENCH_ESTABLISH_4 = 3,
    // Ping packet
    BENCH_PACKET = 4,
    // Request stats and stop the benchmark
    BENCH_REQUEST_REPORT = 5,
    // Statistics from slave
    BENCH_REPORT = 6,
    BENCH_OPCODE_MAX = 6,
};

struct __attribute__((packed)) sfl_establish {
    uint8_t opcode;
    uint32_t timestamp;
    uint32_t payload_size;
};

struct __attribute__((packed)) sfl_packet {
    uint8_t opcode;
    uint32_t timestamp;
    // dynamic length payload, a sequence of increasing bytes with a wrap-around at 255
    uint8_t payload[];
};

struct __attribute__((packed)) sfl_stop {
    uint8_t opcode;
    // dummy fields to ensure same length as s1w_report
    uint32_t count_sent;
    uint32_t dummy2;
    uint32_t dummy3;
};

struct __attribute__((packed)) sfl_report {
    uint8_t opcode;
    uint32_t count_received;
    uint32_t min_latency;
    uint32_t max_latency;
};

/**
 * Handle the slave side of the 1-way benchmark protocol.
 *
 * @param conn - connection to use
 * @param packet - received packet; ensure it's freed at the end of the function
 */
void stress_flood_handle_slave(csp_conn_t *conn, csp_packet_t *packet);

#endif //CSPEMU_CMD_STRESS_FLOOD_H
