#include "console/console.h"
#include "csp/csp_autoconfig.h"

int cmd_csp_buffree(console_ctx_t *ctx, cmd_signature_t *reg);
int cmd_csp_memfree(console_ctx_t *ctx, cmd_signature_t *reg);
int cmd_csp_conn(console_ctx_t *ctx, cmd_signature_t *reg);
int cmd_csp_ifc(console_ctx_t *ctx, cmd_signature_t *reg);
int cmd_csp_rps(console_ctx_t *ctx, cmd_signature_t *reg);
int cmd_csp_uptime(console_ctx_t *ctx, cmd_signature_t *reg);

int cmd_csp_route_list(console_ctx_t *ctx, cmd_signature_t *reg);
int cmd_csp_route_add(console_ctx_t *ctx, cmd_signature_t *reg);
int cmd_csp_route_rm(console_ctx_t *ctx, cmd_signature_t *reg);
int cmd_csp_route_set(console_ctx_t *ctx, cmd_signature_t *reg);

#if CONSOLE_FILE_SUPPORT
int cmd_csp_route_save(console_ctx_t *ctx, cmd_signature_t *reg);
int cmd_csp_route_load(console_ctx_t *ctx, cmd_signature_t *reg);
#endif

int cmd_csp_ping(console_ctx_t *ctx, cmd_signature_t *reg);
int cmd_csp_pt(console_ctx_t *ctx, cmd_signature_t *reg);
int cmd_csp_ident(console_ctx_t *ctx, cmd_signature_t *reg);
int cmd_csp_debug(console_ctx_t *ctx, cmd_signature_t *reg);
int cmd_csp_clock(console_ctx_t *ctx, cmd_signature_t *reg);
int cmd_cmp_ifc(console_ctx_t *ctx, cmd_signature_t *reg);
int cmd_csp_peek(console_ctx_t *ctx, cmd_signature_t *reg);
int cmd_csp_poke(console_ctx_t *ctx, cmd_signature_t *reg);
int cmd_csp_cmp_route_set(console_ctx_t *ctx, cmd_signature_t *reg);
int cmd_csp_rdpopt(console_ctx_t *ctx, cmd_signature_t *reg);
int cmd_csp_reboot(console_ctx_t *ctx, cmd_signature_t *reg);
int cmd_csp_stress_ping(console_ctx_t *ctx, cmd_signature_t *reg);

void console_register_csp_commands(void) {
    console_group_add("cmp", "CSP CMP commands");
    console_group_add("rt", "Manage local CSP routes");

    console_cmd_register(cmd_csp_ping, "ping");
    console_cmd_register(cmd_csp_pt, "pt");

    console_cmd_register(cmd_csp_ident, "ident");
    console_cmd_add_alias_fn(cmd_csp_ident, "cmp ident");

    console_cmd_register(cmd_csp_buffree, "buffree");
    console_cmd_register(cmd_csp_memfree, "memfree");
    console_cmd_register(cmd_csp_ifc, "ifc");
    console_cmd_register(cmd_csp_conn, "conn");
    console_cmd_register(cmd_csp_debug, "debug");
    console_cmd_register(cmd_csp_uptime, "uptime");
    console_cmd_register(cmd_csp_clock, "cmp clock");

    console_cmd_register(cmd_cmp_ifc, "cmp ifc");
    console_cmd_add_alias_fn(cmd_cmp_ifc, "rifc");

    console_cmd_register(cmd_csp_peek, "cmp peek");
    console_cmd_register(cmd_csp_poke, "cmp poke");
    console_cmd_register(cmd_csp_cmp_route_set, "cmp route_set");
    console_cmd_register(cmd_csp_rdpopt, "rdpopt");
    console_cmd_register(cmd_csp_reboot, "reboot");
    console_cmd_register(cmd_csp_rps, "rps");
    console_cmd_register(cmd_csp_stress_ping, "bounce");

#if CSP_USE_CIDR_RTABLE
    console_cmd_register(cmd_csp_route_list, "rt list");
    console_cmd_add_alias_fn(cmd_csp_route_list, "route");

    console_cmd_register(cmd_csp_route_add, "rt add");

    console_cmd_register(cmd_csp_route_rm, "rt rm");
    console_cmd_add_alias_fn(cmd_csp_route_rm, "rt del");

    console_cmd_register(cmd_csp_route_set, "rt set");

    // nested in the cidr test
#if CONSOLE_FILE_SUPPORT
    console_cmd_register(cmd_csp_route_save, "rt save");

    console_cmd_register(cmd_csp_route_load, "rt load");
#endif //CONSOLE_FILE_SUPPORT

#else
    #warning CSP_USE_CIDR_RTABLE is OFF, omitting route management commands!
#endif //CSP_USE_CIDR_RTABLE
}
