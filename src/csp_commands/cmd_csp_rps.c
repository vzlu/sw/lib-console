//
// Remote process list
//

#include "console/cmddef.h"
#include <malloc.h>

// console_print has different signature than CSP expects
void cmd_csp_rps_print(const char* str) {
    console_print(str);
}

int cmd_csp_rps(console_ctx_t *ctx, cmd_signature_t *reg)
{
    (void) ctx; // unused
    static struct {
        struct arg_int *node;
        struct arg_int *timeout;
        struct arg_end *end;
    } args;

    if (reg) {
        args.node = arg_cspaddr1();
        args.timeout = arg_timeout0_def(500);
        args.end = arg_end(2);

        reg->argtable = &args;
        reg->help = EXPENDABLE_STRING("Remote `ps` - list processes of a CSP node");
        return 0;
    }

    // using a short timeout here to prevent the weird delay after rps
    // (it's implemented as a series of {wait-for-msg until timeout})

    int node = args.node->ival[0];

    int timeout = GET_ARG_INT0(args.timeout, 500);

    csp_ps_with_callback(node, timeout, (csp_ps_callback_t)cmd_csp_rps_print);

    return 0;
}
