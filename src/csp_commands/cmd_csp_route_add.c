//
// Add a local CSP route
//

#include <stdlib.h> // atoi
#include "console/cmddef.h"
#include <csp/arch/csp_malloc.h>
#include "x_csp_rt.h"


int cmd_csp_route_add(console_ctx_t *ctx, cmd_signature_t *reg)
{
    (void) ctx; // unused
    static struct {
        struct arg_str *addr;
        struct arg_str *iface;
        struct arg_int *via;
        struct arg_end *end;
    } args;

    if (reg) {
        args.addr = arg_str1(NULL, NULL, "<addr>", EXPENDABLE_STRING("target node address with optional CIDR mask (defaults to /5)"));
        args.iface = arg_str1(NULL, NULL, "<iface>", EXPENDABLE_STRING("interface name (run `ifc` for a list)"));
        args.via = arg_int0("v", "via", "<via>", EXPENDABLE_STRING("next-hop (via) address, default same as addr (255)"));
        args.end = arg_end(4);

        reg->argtable = &args;
        reg->help = EXPENDABLE_STRING("Add a local CSP route");
        return 0;
    }

    x_csp_rtable_t *rt = console_calloc(sizeof(x_csp_rtable_t), 1);
    if (rt == NULL) return CONSOLE_ERR_NO_MEM;

    rt->next = NULL; // it'll be the new tail

    const char *addr = args.addr->sval[0];
    const char *slash = strchr(addr, '/');
    if (NULL == slash) {
        rt->address = (uint8_t) atoi(addr); // FIXME replace with strtol
        rt->netmask = 5;
    } else {
        rt->address = (uint8_t) atoi(addr);
        rt->netmask = (uint8_t) atoi(slash+1);
    }

    rt->interface = csp_iflist_get_by_name((char *) args.iface->sval[0]);
    rt->via = (uint8_t) (args.via->count ? args.via->ival[0] : CSP_NO_VIA_ADDRESS);

    if (rt->interface == NULL) {
        console_printf("Invalid interface!\n");
        console_free(rt);
        return 1;
    }

    x_csp_rtable_t * rtable = x_csp_rtable_import_from_libcsp();

    /* Add entry to linked-list */
    if (rtable == NULL) {
        /* This is the first interface to be added */
        rtable = rt;
    } else {
        /* One or more interfaces were already added */
        x_csp_rtable_t * i = rtable;
        while (i->next)
            i = i->next;
        i->next = rt;
    }

    x_csp_rtable_export_to_libcsp(rtable);

    if (rt->via == CSP_NO_VIA_ADDRESS) {
        console_printf("Added route: %u/%u %s.\n", rt->address, rt->netmask, args.iface->sval[0]);
    } else {
        console_printf("Added route: %u/%u %s %u.\n", rt->address, rt->netmask, args.iface->sval[0], rt->via);
    }
    x_csp_rtable_clear(&rtable);

    // Print the real final state in libcsp
    x_csp_read_and_print_routes();

    //TODO console_printf("Use \"route save\" to persist.\n");

    return 0;
}
