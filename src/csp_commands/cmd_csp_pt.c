//
// Send generic CSP packet
//

#include "console/cmddef.h"
#include <string.h>
#include <stdlib.h>

#define PT_MAX_BODY_SIZE    256

size_t pt_from_hex(const char *hex_string, size_t num_bytes, uint8_t bytes_out[static num_bytes])
{
    const bool has_prefix = (strncmp("0x", hex_string, 2) == 0);
    if (has_prefix) {
        hex_string = hex_string + 2;
    }
    const size_t str_len = strlen(hex_string);
    size_t byte_out_ptr = 0;

    if ((str_len / 2) > num_bytes) {
        return 0;
    }

    for (size_t i = 0; i < str_len; i += 2) {
        const char curr_byte[3] = {
            [0] = hex_string[i],
            [1] = hex_string[i + 1],
            [2] = '\0',
        };
        char *last_char = 0;

        const uint8_t parse_result = (uint8_t)strtoul(curr_byte, &last_char, 16);
        if (*last_char == '\0') {
            bytes_out[byte_out_ptr++] = parse_result;
        } else {
            return 0;
        }
    }

    return str_len/2;
}

int cmd_csp_pt(console_ctx_t *ctx, cmd_signature_t *reg)
{
    (void) ctx; // unused
    static struct {
        struct arg_int *node;
        struct arg_int *port;
        struct arg_str *body;
        struct arg_int *size;
        struct arg_int *timeout;
        struct arg_lit *opt_w;
        struct arg_lit *opt_r;
        struct arg_lit *opt_x;
        struct arg_lit *opt_h;
        struct arg_lit *opt_c;
        struct arg_int *opt_p;
        struct arg_end *end;
    } args;

    if (reg) {
        args.node = arg_cspaddr1();
        args.port = arg_int1(NULL, NULL, "<port>", EXPENDABLE_STRING("Destination port"));
        args.body = arg_str0(NULL, NULL, "<body>", EXPENDABLE_STRING("Wait reply"));
        args.size = arg_int0("s",  NULL, "<bytes>", EXPENDABLE_STRING("Body size"));
        args.timeout = arg_timeout0();
        args.opt_w = arg_lit0("w", NULL, EXPENDABLE_STRING("Wait reply"));
        args.opt_r = arg_lit0("r", NULL, EXPENDABLE_STRING("use RDP"));
        args.opt_x = arg_lit0("x", NULL, EXPENDABLE_STRING("use XTEA"));
        args.opt_h = arg_lit0("H", NULL, EXPENDABLE_STRING("use HMAC"));
        args.opt_c = arg_lit0("c", NULL, EXPENDABLE_STRING("use CRC32"));
        args.opt_p = arg_int0("p", NULL, "<prio>", EXPENDABLE_STRING("Priority"));
        args.end = arg_end(4);

        reg->argtable = &args;
        reg->help = EXPENDABLE_STRING("Pass through command: send generic CSP packet.");
        return 0;
    }

    int node = args.node->ival[0];
    int port = args.port->ival[0];
    int timeout = GET_ARG_TIMEOUT0(args.timeout);
    bool wait_reply = (args.opt_w->count ? true : false);
    size_t body_size = 0;
    size_t ssize = (args.size->count ? args.size->ival[0] : 0);
    uint8_t body[PT_MAX_BODY_SIZE];
    uint32_t options = CSP_O_NONE;
    uint8_t prio = CSP_PRIO_NORM;

    if (args.opt_r->count) options |= CSP_O_RDP;
    if (args.opt_x->count) options |= CSP_O_XTEA;
    if (args.opt_h->count) options |= CSP_O_HMAC;
    if (args.opt_c->count) options |= CSP_O_CRC32;
    if (args.opt_p->count) prio = args.opt_p->ival[0];

    if (args.body->count) {
        body_size = pt_from_hex(args.body->sval[0], PT_MAX_BODY_SIZE, body);
    }



    /* for debug only
    console_printf("Pass through: node %d, port: %d, timeout %u ms, wait:%d, body[%d]:\n",
                   node, port, timeout, wait_reply ? 1 : 0, (int)body_size);
    if (body_size) {
        console_hexdump(body, body_size);
    }
    */

    size_t aloc = body_size > ssize ? body_size : ssize;
    csp_packet_t * packet = csp_buffer_get(aloc);
    if (packet == NULL) {
        return CONSOLE_ERR_NO_MEM;
    }
    memset(packet->data, 0, aloc);

    csp_conn_t * conn = csp_connect(prio, node, port, timeout, options);
    if (!conn) {
        console_printf("conn\n");
        return CONSOLE_ERR_IO;
    }

    packet->length = aloc;
    if (body_size > 0)
        memcpy(packet->data, body, body_size);

    if (!csp_send(conn, packet, timeout)) {
        console_printf("send\n");
        csp_buffer_free(packet);
        csp_close(conn);
        return CONSOLE_ERR_IO;
    }

    if (!wait_reply) {
        csp_close(conn);
        return CONSOLE_OK;
    }

    packet = csp_read(conn, timeout);
    if (!packet) {
        console_printf("read\n");
        csp_close(conn);
        return CONSOLE_ERR_IO;
    }

    console_printf("rpl[%d]:\n", packet->length);
    if (packet->length) {
        console_hexdump(packet->data, packet->length);
    }

    csp_buffer_free(packet);
    csp_close(conn);

    return CONSOLE_OK;
}
