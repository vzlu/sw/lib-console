//
// Show local CSP connection stats
//

#include "console/cmddef.h"
#include <malloc.h>
#include <csp/csp.h>

int cmd_csp_conn(console_ctx_t *ctx, cmd_signature_t *reg)
{
    (void) ctx; // unused
    if (reg) {
        reg->help = EXPENDABLE_STRING("Show CSP connection table");
        return 0;
    }

    // prints up to 10 connections, each up to 100 chars long

    char *buf = console_calloc(1024, 1);
    if (!buf) return CONSOLE_ERR_NO_MEM;

    csp_conn_print_table_str(buf, 1024);

    console_print(buf);
    console_free(buf);

    return 0;
}
