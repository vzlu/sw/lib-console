//
// Configure CSP RDP
//

#include <csp/csp.h>
#include "console/cmddef.h"

/** weak hook capturing RDPOPT change, can be used for persistence */
void __attribute__((weak)) cmd_csp_rdpopt_hook(unsigned int window_size, unsigned int conn_timeout_ms,
                                               unsigned int packet_timeout_ms, unsigned int delayed_acks,
                                               unsigned int ack_timeout, unsigned int ack_delay_count) {
    // ...
}

int cmd_csp_rdpopt(console_ctx_t *ctx, cmd_signature_t *reg)
{
    (void) ctx; // unused
    static struct {
        struct arg_int *window_size;
        struct arg_int *conn_timeout;
        struct arg_int *packet_timeout;
        struct arg_int *delayed_acks;
        struct arg_int *ack_timeout;
        struct arg_int *ack_delay_count;
        struct arg_end *end;
    } args;

    if (reg) {
        args.window_size = arg_int0("w", "wsz", "<n>", EXPENDABLE_STRING("window size"));
        args.conn_timeout = arg_int0("c", "cto", "<n>", EXPENDABLE_STRING("connection timeout"));
        args.packet_timeout = arg_int0("p", "pto", "<n>", EXPENDABLE_STRING("packet timeout"));
        args.delayed_acks = arg_int0("d", "dack", "<n>", EXPENDABLE_STRING("delayed acks"));
        args.ack_timeout = arg_int0("a", "ato", "<n>", EXPENDABLE_STRING("ack timeout"));
        args.ack_delay_count = arg_int0("D", "adc", "<n>", EXPENDABLE_STRING("ack delay count"));
        args.end = arg_end(6);

        reg->argtable = &args;
        reg->help = EXPENDABLE_STRING("Set local RDP options");
        return 0;
    }

    unsigned int window_size;
    unsigned int conn_timeout;
    unsigned int packet_timeout;
    unsigned int delayed_acks;
    unsigned int ack_timeout;
    unsigned int ack_delay_count;

    csp_rdp_get_opt(&window_size, &conn_timeout, &packet_timeout,
                    &delayed_acks, &ack_timeout, &ack_delay_count);

    if (args.window_size->count)
        window_size = (unsigned int) args.window_size->ival[0];
    if (args.conn_timeout->count)
        conn_timeout = (unsigned int) args.conn_timeout->ival[0];
    if (args.packet_timeout->count)
        packet_timeout = (unsigned int) args.packet_timeout->ival[0];
    if (args.delayed_acks->count)
        delayed_acks = (unsigned int) args.delayed_acks->ival[0];
    if (args.ack_timeout->count)
        ack_timeout = (unsigned int) args.ack_timeout->ival[0];
    if (args.ack_delay_count->count)
        ack_delay_count = (unsigned int) args.ack_delay_count->ival[0];

    console_printf("Setting arguments to: window size %u, conn timeout %u, packet timeout %u, delayed acks %u, ack timeout %u, ack delay count %u\n", window_size, conn_timeout, packet_timeout,
                   delayed_acks, ack_timeout, ack_delay_count);

    csp_rdp_set_opt(window_size, conn_timeout, packet_timeout,
                    delayed_acks, ack_timeout, ack_delay_count);

    cmd_csp_rdpopt_hook(window_size, conn_timeout, packet_timeout,
                        delayed_acks, ack_timeout, ack_delay_count);

    return 0;
}
