/**
 * CSP route editing
 *
 * This module duplicates some code from libcsp, because the routing table internals
 * are not exposed sufficiently.
 *
 * To use this, have libcsp export its routing table, and import it here.
 * The reverse process is used to update the libcsp version.
 * 
 * Created on 2020/03/06.
 */

#ifndef VCOM_X_CSP_RT_H
#define VCOM_X_CSP_RT_H

#include <stdint.h>

#include <csp/csp_types.h>
#include <csp/csp_iflist.h>
#include <csp/csp_rtable.h>

/* Local typedef for routing table */
typedef struct __attribute__((__packed__)) csp_rtable_s {
    uint8_t address;
    uint8_t netmask;
    uint8_t via;
    csp_iface_t * interface;
    struct csp_rtable_s * next;
} x_csp_rtable_t;

#define x_RTABLE_BUFLEN CSP_RTABLE_LOAD_MAX_STRLEN
extern char x_rtable_buffer[x_RTABLE_BUFLEN];


x_csp_rtable_t * x_csp_rtable_import_from_libcsp();

void x_csp_rtable_export_to_libcsp(x_csp_rtable_t *rtable);

void x_csp_rtable_clear(x_csp_rtable_t **rtable);
void console_print_routes(x_csp_rtable_t *table);
int x_csp_rtable_parse(x_csp_rtable_t ** table, char * buffer);
int x_csp_rtable_save(x_csp_rtable_t *rtable, char * buffer, int maxlen);
int x_csp_rtable_set(x_csp_rtable_t **rtable, uint8_t _address, uint8_t _netmask, csp_iface_t *ifc, uint8_t mac);
x_csp_rtable_t * x_csp_rtable_find(x_csp_rtable_t *table, uint8_t addr, uint8_t netmask, uint8_t exact);

void x_csp_read_and_print_routes();

#endif //VCOM_X_CSP_RT_H
