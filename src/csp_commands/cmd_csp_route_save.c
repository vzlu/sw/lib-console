//
// Save routing table to file
//

#include "console/cmddef.h"
#include "x_csp_rt.h"
#include <errno.h>
#include <unistd.h>

/**
 * Default implementation of `cmd_csp_route_save_hook`
 * - can be called from user code overriding the weak stub
 */
console_err_t cmd_csp_route_save_hook_default(const char *exported, const char *fname) {
    FILE *f = fopen(fname, "w+");
    if (f == NULL) {
        console_printf("Error opening file: %s", strerror(errno));
        return CONSOLE_ERR_IO;
    } else {
        ftruncate(fileno(f), 0);
        fwrite(exported, 1, strlen(exported), f);
        fclose(f);
    }

    return CONSOLE_OK;
}

/**
 * Save routes to a file.
 *
 * This default implementation has weak linkage to allow overriding.
 *
 * @param[in] exported - the route table string exported from CSP
 * @param[in] fname - specified file name
 */
console_err_t __attribute__((weak)) cmd_csp_route_save_hook(const char *exported, const char *fname) {
    return cmd_csp_route_save_hook_default(exported, fname);
}

int cmd_csp_route_save(console_ctx_t *ctx, cmd_signature_t *reg)
{
    (void) ctx; // unused
    static struct {
        struct arg_file *file;
        struct arg_end *end;
    } args;

    if (reg) {
        args.file = arg_file0(NULL, NULL, "<file>", EXPENDABLE_STRING("destination file, defaults to \"routes.txt\""));
        args.end = arg_end(1);

        reg->argtable = &args;
        reg->help = EXPENDABLE_STRING("Save the routing table to a file");
        return 0;
    }

    csp_rtable_save(x_rtable_buffer, x_RTABLE_BUFLEN);

    const char *fname = args.file->count ?
                        args.file->filename[0] : "routes.txt";

    console_printf("Saving routing table to file: %s\n%s\n", fname, x_rtable_buffer);
    return cmd_csp_route_save_hook(x_rtable_buffer, fname);
}
