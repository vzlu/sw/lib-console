//
// Toggle CSP debug levels
//

#include <console/prefix_match.h>
#include <ctype.h>
#include "console/cmddef.h"

struct str2level {
    const char *str;
    csp_debug_level_t value;
};

#define LEVEL_COUNT 8
static const struct str2level levels[LEVEL_COUNT] = {
    {"error",    CSP_ERROR},
    {"warning",  CSP_WARN},
    {"info",     CSP_INFO},
    {"buffer",   CSP_BUFFER},
    {"packet",   CSP_PACKET},
    {"protocol", CSP_PROTOCOL},
    {"lock",     CSP_LOCK},
    {"hexdump",  CSP_HEXDUMP},
};

static const char *level_names[LEVEL_COUNT + 1] = {
    [CSP_ERROR] = "error", /* 0 */
    [CSP_WARN] = "warning",
    [CSP_INFO] = "info",
    [CSP_BUFFER] = "buffer",
    [CSP_PACKET] = "packet",
    [CSP_PROTOCOL] = "protocol",
    [CSP_LOCK] = "lock",
    [CSP_HEXDUMP] = "hexdump", /* 7 */
    NULL
};

/**
 * Print one line of the levels dump, with colors if enabled.
 * @param level
 */
static void dump_csp_debug_level(int level)
{
    bool enabled = csp_debug_get_level(levels[level].value);

    if (console_active_ctx->use_colors) {
        console_printf("CSP debug['%s'] = ", levels[level].str);
        if (enabled) {
            console_color_printf(COLOR_GREEN, "On\n");
        } else {
            console_color_printf(COLOR_RED, "Off\n");
        }
    } else {
        console_printf("CSP debug['%s'] = %s", levels[level].str, enabled ? "On" : "Off");
    }
}

// this is not static to allow external calls
void dump_csp_debug_levels(void)
{
    for (int i = 0; i < LEVEL_COUNT; i++) {
        dump_csp_debug_level(i);
    }
}

// weak linkage change handler to allow persistence
extern void __attribute__((weak)) cmd_csp_debug_hook(void);

int cmd_csp_debug(console_ctx_t *ctx, cmd_signature_t *reg)
{
    (void) ctx; // unused
    static struct {
        struct arg_str *level;
        struct arg_str *cmd;
        struct arg_end *end;
    } args;

    if (reg) {
        args.level = arg_str0(NULL, NULL, "<type>", EXPENDABLE_STRING("log type (error,warning,info,buffer,packet,protocol,lock) - first few letters suffice. Omit to print all levels"));
        args.cmd = arg_str0(NULL, NULL, "<onoff>", EXPENDABLE_STRING("enable / disable logging. Omit to check state"));
        args.end = arg_end(2);

        reg->argtable = &args;
        reg->help = EXPENDABLE_STRING("Check or toggle CSP log levels. Run with no args to print all levels");
        return 0;
    }

    if (args.level->count == 0) {
        dump_csp_debug_levels();
        return 0;
    }

    const char *level_s = args.level->sval[0];

    int level = prefix_match(level_s, level_names, 0);
    if (level < 0) {
        console_color_printf(COLOR_RED, "Unknown or ambiguous debug level: %s\n", level_s);
        return CONSOLE_ERR_INVALID_ARG;
    }

    if (args.cmd->count) {
        const char *cmd_s = args.cmd->sval[0];
        bool enable = false;
        switch (tolower(cmd_s[0])) {
            case 'y': // yes
            case 'e': // enable
            case '1':
                enable = true;
                break;
            case 'n': // no
            case 'd': // disable
            case '0':
                enable = false;
                break;
            default:
                if(0 == strcasecmp(cmd_s, "on")) {
                    enable = true;
                } else if(0 == strcasecmp(cmd_s, "off")) {
                    enable = false;
                } else {
                    console_color_printf(COLOR_RED, "Bad arg: %s\n", cmd_s);
                    return CONSOLE_ERR_INVALID_ARG;
                }
        }

        csp_debug_set_level(level, enable);

        console_print("SET: ");
        dump_csp_debug_level(level);

        if (cmd_csp_debug_hook) {
            cmd_csp_debug_hook();
        }
    }
    else {
        dump_csp_debug_level(level);
    }

    return 0;
}
