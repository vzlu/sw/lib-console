//
// Read remote memory
//

#include "console/cmddef.h"
#include <csp/csp_cmp.h>
#include <csp/csp_endian.h>


int cmd_csp_peek(console_ctx_t *ctx, cmd_signature_t *reg)
{
    (void) ctx; // unused
    static struct {
        struct arg_int *node;
        struct arg_int *timeout;
        struct arg_str *addr;
        struct arg_int *len;
        struct arg_end *end;
    } args;

    if (reg) {
        args.node = arg_cspaddr1();
        args.timeout = arg_timeout0();
        args.addr = arg_str1(NULL, NULL, "<addr>", EXPENDABLE_STRING("address (hex without 0x)"));
        args.len = arg_int1(NULL, NULL, "<len>", EXPENDABLE_STRING("length"));
        args.end = arg_end(3);

        reg->argtable = &args;
        reg->help = EXPENDABLE_STRING("Peek CSP node memory via CMP");
        return 0;
    }

    int addr, len;

    int node = args.node->ival[0];
    int timeout = GET_ARG_TIMEOUT0(args.timeout);

    if (sscanf(args.addr->sval[0], "%x", &addr) != 1)
        return 1;

    len = args.len->ival[0];

    if (len > CSP_CMP_PEEK_MAX_LEN) {
        console_printf("Max length is: %u\n", CSP_CMP_PEEK_MAX_LEN);
        return 1;
    }

    console_printf("Dumping mem from node %u addr 0x%x len %u timeout %d\n", node, addr, len, timeout);

    struct csp_cmp_message msg;
    msg.peek.addr = csp_hton32(addr);
    msg.peek.len = len;

    int ret = csp_cmp_peek(node, timeout, &msg);
    if (ret != CSP_ERR_NONE) {
        console_printf("Error: %d\n", ret);
        return 1;
    }

    console_hexdump(msg.peek.data, len);

    return 0;
}
