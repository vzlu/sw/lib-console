//
// Show CSP node's memory stats
//

#include "console/cmddef.h"
#include <malloc.h>

int cmd_csp_memfree(console_ctx_t *ctx, cmd_signature_t *reg)
{
    (void) ctx; // unused
    static struct {
        struct arg_int *node;
        struct arg_int *timeout;
        struct arg_end *end;
    } args;

    if (reg) {
        args.node = arg_cspaddr0();
        args.timeout = arg_timeout0();
        args.end = arg_end(2);

        reg->argtable = &args;
        reg->help = EXPENDABLE_STRING("Check free memory of a CSP node");
        return 0;
    }

    int timeout = GET_ARG_TIMEOUT0(args.timeout);
    int node = GET_ARG_CSPADDR0(args.node);

    uint32_t memfree;
    int rv = csp_get_memfree(node, timeout, &memfree);
    if (rv == CSP_ERR_NONE) {
        console_printf("Free Memory at node %u is %"PRIu32" bytes\n", node, memfree);
    } else {
        console_printf("CSP network error: %d\n", rv);
    }

    return 0;
}
