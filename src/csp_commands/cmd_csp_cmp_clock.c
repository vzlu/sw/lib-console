//
// Set node clock via CSP CMP
//

#include "console/cmddef.h"
#include <csp/csp_cmp.h>
#include <csp/csp_endian.h>
#include <csp/arch/csp_clock.h>

int cmd_csp_clock(console_ctx_t *ctx, cmd_signature_t *reg)
{
    (void) ctx; // unused
    static struct {
        struct arg_int *node;
        struct arg_int *timeout;
        struct arg_int *sec;
        struct arg_int *nsec;
        struct arg_end *end;
    } args;

    if (reg) {
        args.node = arg_cspaddr1();
        args.timeout = arg_timeout0();
        args.sec = arg_int0("s", "s", "<sec>", EXPENDABLE_STRING("seconds"));
        args.nsec = arg_int0("n", "ns", "<nsec>", EXPENDABLE_STRING("nanoseconds"));
        args.end = arg_end(4);

        reg->argtable = &args;
        reg->help = EXPENDABLE_STRING("Set CSP node clock via CMP");
        return 0;
    }

    int node = args.node->ival[0];
    int timeout = GET_ARG_TIMEOUT0(args.timeout);

    int sec=0, nsec=0;
    bool setting = false;
    if (args.sec->count) {
        sec = args.sec->ival[0];
        if (sec < 0) { // get time from local OS
            #if !defined(OBC_FIRMWARE)
            sec = time(0);
            #else
            csp_timestamp_t csp_time;
            csp_clock_get_time(&csp_time);
            sec = csp_time.tv_sec;
            #endif
        }
        setting = true;
    }
    if (args.nsec->count) {
        nsec = args.nsec->ival[0];
        setting = true;
    }

    struct csp_cmp_message msg = {};

    msg.clock.tv_sec = sec;
    msg.clock.tv_nsec = nsec;

    if (!setting) {
        if (csp_cmp_clock(node, timeout, &msg) != CSP_ERR_NONE)
            return 1;
        console_printf("Node %d, got clock: ", node);
    } else {
        msg.clock.tv_sec = csp_hton32(msg.clock.tv_sec);
        msg.clock.tv_nsec = csp_hton32(msg.clock.tv_nsec);
        if (csp_cmp_clock(node, timeout, &msg) != CSP_ERR_NONE)
            return 1;
        console_printf("Node %d clock set: ", node);
    }

    msg.clock.tv_sec = csp_ntoh32(msg.clock.tv_sec);
    msg.clock.tv_nsec = csp_ntoh32(msg.clock.tv_nsec);

    time_t raw_time;
    struct tm * timeinfo;
    char buf[25];

    raw_time = msg.clock.tv_sec;
    timeinfo = localtime (&raw_time);
    strftime (buf,25,"%F %T",timeinfo);

    console_printf("%s.%06"PRIu32"\n", buf, msg.clock.tv_nsec / 1000);

    return 0;
}
