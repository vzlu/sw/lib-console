//
// Remove a local routing table
//

#include "console/cmddef.h"
#include <csp/arch/csp_malloc.h>
#include "x_csp_rt.h"

int cmd_csp_route_rm(console_ctx_t *ctx, cmd_signature_t *reg)
{
    (void) ctx; // unused
    static struct {
        struct arg_int *num;
        struct arg_end *end;
    } args;

    if (reg) {
        args.num = arg_int1(NULL, NULL, "<num>", EXPENDABLE_STRING("route number in the routing table"));
        args.end = arg_end(1);

        reg->argtable = &args;
        reg->help = EXPENDABLE_STRING("Delete a route by its number (see `route`)");
        return 0;
    }

    const int num = args.num->ival[0];

    x_csp_rtable_t * rtable = x_csp_rtable_import_from_libcsp();

    int j = 1;
    x_csp_rtable_t * head = NULL;
    x_csp_rtable_t * rt = NULL;
    for (rt = rtable; (rt); rt = rt->next) {
        if (j == num) {
            break;
        }
        head = rt;
        j++;
    }
    if (rt == NULL) {
        console_printf("Invalid number. See route list for route numbers.\n");
        x_csp_rtable_clear(&rtable);
        return CONSOLE_ERR_INVALID_ARG;
    }

    if (head == NULL) {
        // this was the first route
        rtable = rt->next;
    } else {
        head->next = rt->next;
    }

    x_csp_rtable_export_to_libcsp(rtable);

    csp_free(rt);
    console_printf("Route #%d removed.\n", num);
    x_csp_rtable_clear(&rtable);

    // Print the real final state in libcsp
    x_csp_read_and_print_routes();

    //TODO console_printf("Use \"route save\" to persist.\n");

    return 0;
}
