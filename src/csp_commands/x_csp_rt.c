#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <csp/csp_types.h>
#include <csp/csp_iflist.h>
#include <csp/csp_rtable.h>
#include <csp/csp_error.h>
#include <csp/arch/csp_malloc.h>
#include "console/cmddef.h"

#include "x_csp_rt.h"

char x_rtable_buffer[x_RTABLE_BUFLEN];

x_csp_rtable_t * x_csp_rtable_import_from_libcsp() {
    x_rtable_buffer[0] = 0;
    csp_rtable_save(x_rtable_buffer, x_RTABLE_BUFLEN);
    x_csp_rtable_t * dest = NULL;
    x_csp_rtable_parse(&dest, (char *) x_rtable_buffer); // missing const in libcsp
    return dest;
}

void x_csp_rtable_export_to_libcsp(x_csp_rtable_t *rtable) {
    x_rtable_buffer[0] = 0;
    x_csp_rtable_save(rtable, x_rtable_buffer, x_RTABLE_BUFLEN);
    csp_rtable_clear();
    csp_rtable_load(x_rtable_buffer);
}

void x_csp_rtable_clear(x_csp_rtable_t **rtable) {
    for (x_csp_rtable_t * i = *rtable; (i);) {
        void * freeme = i;
        i = i->next;
        csp_free(freeme);
    }
    *rtable = NULL;
}

void console_print_routes(x_csp_rtable_t *table)
{
    int num = 1;
    console_println("Nr Addr/Mask Iface Via");
    for (x_csp_rtable_t * i = table; (i); i = i->next) {
        if (i->via == CSP_NO_VIA_ADDRESS) {
            console_printf("#%d: %u/%u %s\n", num, i->address, i->netmask, i->interface->name);
        } else {
            console_printf("#%d: %u/%u %s %u\n", num, i->address, i->netmask, i->interface->name, i->via);
        }

        num++;
    }
    if (num == 1) {
        console_println("No routes defined.");
    }
}

void x_csp_read_and_print_routes() {
    console_println("--- CSP routes ---");
    x_csp_rtable_t * rtable = x_csp_rtable_import_from_libcsp();
    console_print_routes(rtable);
    x_csp_rtable_clear(&rtable);
}

int x_csp_rtable_set(x_csp_rtable_t **rtable, uint8_t _address, uint8_t _netmask, csp_iface_t *ifc, uint8_t mac) {

    if (ifc == NULL)
        return CSP_ERR_INVAL;

    /* Set default route in the old way */
    int address = 0;
    int netmask = 0;
    if (_address == CSP_DEFAULT_ROUTE) {
        netmask = 0;
        address = 0;
    } else {
        netmask = _netmask;
        address = _address;
    }

    /* Fist see if the entry exists */
    x_csp_rtable_t * entry = x_csp_rtable_find(*rtable, address, netmask, 1);

    /* If not, create a new one */
    if (!entry) {
        entry = console_calloc(sizeof(x_csp_rtable_t), 1);
        if (entry == NULL)
            return CSP_ERR_NOMEM;

        entry->next = NULL;
        /* Add entry to linked-list */
        if (*rtable == NULL) {
            /* This is the first interface to be added */
            *rtable = entry;
        } else {
            /* One or more interfaces were already added */
            x_csp_rtable_t * i = *rtable;
            while (i->next)
                i = i->next;
            i->next = entry;
        }
    }

    /* Fill in the data */
    entry->address = address;
    entry->netmask = netmask;
    entry->interface = ifc;
    entry->via = mac;

    return CSP_ERR_NONE;
}



x_csp_rtable_t * x_csp_rtable_find(x_csp_rtable_t *table, uint8_t addr, uint8_t netmask, uint8_t exact) {

    /* Remember best result */
    x_csp_rtable_t * best_result = NULL;
    uint8_t best_result_mask = 0;

    /* Start search */
    x_csp_rtable_t * i = table;
    while(i) {

        /* Look for exact match */
        if (i->address == addr && i->netmask == netmask) {
            best_result = i;
            break;
        }

        /* Try a CIDR netmask match */
        if (!exact) {
            uint8_t hostbits = (1 << (CSP_ID_HOST_SIZE - i->netmask)) - 1;
            uint8_t netbits = ~hostbits;
            //printf("Netbits %x Hostbits %x\n", netbits, hostbits);

            /* Match network addresses */
            uint8_t net_a = i->address & netbits;
            uint8_t net_b = addr & netbits;
            //printf("A: %hhx, B: %hhx\n", net_a, net_b);

            /* We have a match */
            if (net_a == net_b) {
                if (i->netmask >= best_result_mask) {
                    //printf("Match best result %u %u\n", best_result_mask, i->netmask);
                    best_result = i;
                    best_result_mask = i->netmask;
                }
            }

        }

        i = i->next;

    }

    return best_result;

}

int x_csp_rtable_parse(x_csp_rtable_t ** table, char * buffer) {

    int valid_entries = 0;

    console_printf("Routes string from libcsp: %s\n", buffer);

    /* Copy string before running strtok */
    char * str = console_malloc(strlen(buffer) + 1);
    char * const buffer_copy = str; // str is modified by strtok, keep the original pointer for free
    if (!str) return CONSOLE_ERR_NO_MEM;
    memcpy(str, buffer, strlen(buffer) + 1);

    /* Get first token */
    str = strtok(str, ",");

    while ((str) && (strlen(str) > 1)) {
        int address = 0;
        int netmask = 0;
        int via = 255;
        char name[100] = {};

        if (sscanf(str, "%u/%u %14s %u", &address, &netmask, name, &via) == 4) {
        } else if (sscanf(str, "%u/%u %14s", &address, &netmask, name) == 3) {
            via = CSP_NO_VIA_ADDRESS;
        } else if (sscanf(str, "%u %14s %u", &address, name, &via) == 3) {
            netmask = CSP_ID_HOST_SIZE;
        } else if (sscanf(str, "%u %14s", &address, name) == 2) {
            netmask = CSP_ID_HOST_SIZE;
            via = CSP_NO_VIA_ADDRESS;
        } else {
            // invalid entry
            console_printf("Parse error %s\n", str);
            goto next;
        }

        //printf("Parsed %u/%u %u %s\n", address, netmask, mac, name);
        csp_iface_t * ifc = csp_iflist_get_by_name(name);
        if (ifc) {
            x_csp_rtable_set(table, address, netmask, ifc, via);
        } else {
            console_printf("Unknown interface %s\n", name);
            goto next;
        }
        valid_entries++;
    next:
        str = strtok(NULL, ",");
    }

    console_free(buffer_copy);
    return valid_entries;
}

int x_csp_rtable_save(x_csp_rtable_t *rtable, char * buffer, int maxlen) {
    int len = 0;
    for (x_csp_rtable_t * i = rtable; (i); i = i->next) {
        if (len >= maxlen-2) {
            console_printf("Route list too long!\n");
            break;
        }
        if (len > 0) {
            buffer[len] = ',';
            len++;
        }
        len += snprintf(buffer + len, maxlen - len, "%u", i->address);
        if (i->netmask != 5) len += snprintf(buffer + len, maxlen - len, "/%u", i->netmask);
        len += snprintf(buffer + len, maxlen - len, " %s", i->interface->name);
        if (i->via != CSP_NODE_MAC) len += snprintf(buffer + len, maxlen - len, " %u", i->via);
    }
    console_printf("Route string for CSP: %s\n", buffer);
    return len;
}
