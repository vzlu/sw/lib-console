//
// Reboot a CSP node
//

#include <csp/csp.h>
#include "console/cmddef.h"

int cmd_csp_reboot(console_ctx_t *ctx, cmd_signature_t *reg)
{
    (void) ctx; // unused
    static struct {
        struct arg_int *node;
        struct arg_end *end;
    } args;

    if (reg) {
        args.node = arg_cspaddr0();
        args.end = arg_end(1);

        reg->argtable = &args;
        reg->help = EXPENDABLE_STRING("Reboot a CSP node");
        return 0;
    }

    int node = GET_ARG_CSPADDR0(args.node);
    csp_reboot(node);

    return 0;
}
