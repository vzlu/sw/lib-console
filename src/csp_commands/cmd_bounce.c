//
// Stress-testing command
//

#include "console/cmddef.h"
#include <csp/arch/csp_time.h>
#include <csp/arch/csp_thread.h>
#include <limits.h>

#define DEF_INTERVAL_MS 0

/** 'stress' command stress-tests an interface */
int cmd_csp_stress_ping(console_ctx_t *ctx, cmd_signature_t *reg)
{
    (void) ctx; // unused
    static struct {
        struct arg_int *node;
        struct arg_int *timeout;
        struct arg_int *interval;
        struct arg_int *size;
        struct arg_int *count;
        struct arg_lit *opt_r;
        struct arg_lit *opt_x;
        struct arg_lit *opt_h;
        struct arg_lit *opt_c;
        struct arg_lit *quiet;
        struct arg_end *end;
    } args;

    if (reg) {
        args.node = arg_cspaddr1();
        args.timeout = arg_int0("t", "timeout", "<ms>", EXPENDABLE_STRING("ping timeout in ms (default "STR(CONSOLE_CSP_DEF_TIMEOUT_MS)")"));
        args.interval = arg_int0("n", "period", "<ms>", EXPENDABLE_STRING("ping interval in ms (default "STR(DEF_INTERVAL_MS)")"));
        args.size = arg_int0("s", "size", "<bytes>", EXPENDABLE_STRING("payload size"));
        args.count = arg_int0("i", "count", "<count>", EXPENDABLE_STRING("max ping count (can be interrupted by hitting a key)"));
        args.opt_r = arg_lit0("r", NULL, EXPENDABLE_STRING("use RDP"));
        args.opt_x = arg_lit0("x", NULL, EXPENDABLE_STRING("use XTEA"));
        args.opt_h = arg_lit0("H", NULL, EXPENDABLE_STRING("use HMAC"));
        args.opt_c = arg_lit0("c", NULL, EXPENDABLE_STRING("use CRC32"));
        args.quiet = arg_lit0("q", NULL, EXPENDABLE_STRING("quiet (suppress ping logging)"));
        args.end = arg_end(8);

        reg->argtable = &args;
        reg->help = EXPENDABLE_STRING("Measure CSP connection round-trip latencies and packet loss");
        return 0;
    }

    const int node = args.node->ival[0];
    const int timeout = GET_ARG_INT0(args.timeout, CONSOLE_CSP_DEF_TIMEOUT_MS);
    const int interval = GET_ARG_INT0(args.interval, DEF_INTERVAL_MS);

    uint32_t options = CSP_O_NONE;
    if (args.opt_r->count) options |= CSP_O_RDP;
    if (args.opt_x->count) options |= CSP_O_XTEA;
    if (args.opt_h->count) options |= CSP_O_HMAC;
    if (args.opt_c->count) options |= CSP_O_CRC32;

    const int size = (args.size->count) ? args.size->ival[0] : 1;

    console_printf("Starting ping test (target: %d, len: %d, ival: %d ms, tout: %d ms).\n",
                   node, size, interval, timeout);

    if (console_have_stdin()) {
        console_printf("Press any key to stop.\n");
    }

    size_t ping_count = 0;
    size_t lost_count = 0;
    int min_time = INT_MAX;
    int max_time = 0;

    // if STDIN is available, use infinity as default. If no input is available to press "any key",
    // use a fixed number.
    size_t max_count = args.count->count ?
        args.count->ival[0] :
        (console_have_stdin() ? 0 /* unlimited */ : 10);

    bool quiet = args.quiet->count > 0;

    const uint32_t start_time = csp_get_ms();
    while (1) {
        ping_count++;

        int ping_time = csp_ping(node, timeout, size, options);

        if (ping_time < 0) {
            lost_count++;
            console_printf("Timeout, or sending failed!\n");
        } else if (ping_time <= 1) {
            if (!quiet) console_printf("Reply in <1 tick\n");
            ping_time = 0;
        } else {
            if (!quiet) console_printf("Reply in %u ms\n", (unsigned int) ping_time);
        }

        if (ping_time > 0) {
            if (ping_time < min_time) {
                min_time = ping_time;
            }
            else if (ping_time > max_time) {
                max_time = ping_time;
            }
        }

        // Exit if any key pressed
        if (console_have_stdin() && console_can_read()) {
            // discard the key
            char tmp = 0;
            console_read(&tmp, 1);

            // Rx of zero happens with telnet
            if (tmp != 0) {
                break;
            }
        }

        // exit if the max number of pings was sent
        if (max_count && (ping_count >= max_count)) {
            break;
        }

        if (ping_time < interval) {
            csp_sleep_ms(interval - ping_time);
        }
    }
    const uint32_t end_time = csp_get_ms();
    const uint32_t elapsed = end_time - start_time;

    const size_t suc_count = ping_count - lost_count;
    const uint64_t raw_bytes = size * suc_count;
    const float byterate = ((float)raw_bytes / elapsed) * 1000.0f;

    console_printf("Test ended.\n"
                   "--- Stats ---\n"
                   "Pings sent: %d\n"
                   "Timed out: %d (loss %.2f%%)\n"
                   "Data rate: %.1f B/s (one line)\n",
                   (int)ping_count,
                   (int)lost_count, 100.0f * (lost_count / (float) ping_count),
                   byterate);

    if (lost_count != ping_count) {
        console_printf("RTT: %d - %d ms\n",
                       (int)min_time, (int)max_time);
    } else {
        console_printf("RTT: N/A\n");
    }

    return 0;
}
