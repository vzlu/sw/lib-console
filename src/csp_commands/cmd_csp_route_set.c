//
// Modify a local CSP route
//

#include <stdlib.h>
#include "console/cmddef.h"
#include "x_csp_rt.h"


int cmd_csp_route_set(console_ctx_t *ctx, cmd_signature_t *reg)
{
    (void) ctx; // unused
    static struct {
        struct arg_int *num;
        struct arg_str *addr;
        struct arg_str *iface;
        struct arg_int *via;
        struct arg_end *end;
    } args;

    if (reg) {
        args.num = arg_int1(NULL, NULL, "<num>", EXPENDABLE_STRING("route number in the routing table"));
        args.addr = arg_str0("a", "addr", "<addr>", EXPENDABLE_STRING("target node address with optional CIDR mask (defaults to /5)"));
        args.iface = arg_str0("i", "if", "<iface>", EXPENDABLE_STRING("interface name (run `ifc` for a list)"));
        args.via = arg_int0("v", "via", "<via>", EXPENDABLE_STRING("next-hop (via) address, default same as addr (255)"));
        args.end = arg_end(4);

        reg->argtable = &args;
        reg->help = EXPENDABLE_STRING("Modify a local CSP route identified by its number (see `route`). "
                    "All arguments are optional; original value is omitted.");
        return 0;
    }

    const int num = args.num->ival[0];

    x_csp_rtable_t * rtable = x_csp_rtable_import_from_libcsp();

    int j = 1;
    x_csp_rtable_t * rt = NULL;
    for (rt = rtable; (rt); rt = rt->next) {
        if (j == num) {
            break;
        }
        j++;
    }
    if (rt == NULL) {
        console_printf("Invalid number. See route list for route numbers.\n");
        x_csp_rtable_clear(&rtable);
        return CONSOLE_ERR_INVALID_ARG;
    }

    // avoid setting interface to a invalid value in the routing table
    csp_iface_t * ifc = rt->interface;

    if (args.addr->count) {
        const char *addr = args.addr->sval[0];
        const char *slash = strchr(addr, '/');
        if (NULL == slash) {
            rt->address = (uint8_t) atoi(addr); // FIXME replace with strtol
            rt->netmask = 5;
        }
        else {
            rt->address = (uint8_t) atoi(addr);
            rt->netmask = (uint8_t) atoi(slash + 1);
        }
    }

    if (args.iface->count) ifc = csp_iflist_get_by_name((char *) args.iface->sval[0]);
    if (args.via->count) rt->via = (uint8_t) args.via->ival[0];

    if (ifc == NULL) {
        console_printf("Invalid interface!\n");
        x_csp_rtable_clear(&rtable);
        return CONSOLE_ERR_INVALID_ARG;
    }
    rt->interface = ifc;

    x_csp_rtable_export_to_libcsp(rtable);

    console_printf("Route modified.\n");
    x_csp_rtable_clear(&rtable);

    // Print the real final state in libcsp
    x_csp_read_and_print_routes();

    //TODO console_printf("Use \"route save\" to persist.\n");
    return 0;
}
