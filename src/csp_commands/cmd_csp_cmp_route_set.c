//
// Send route-set CSP CMP command
//

#include "console/cmddef.h"
#include <csp/csp_cmp.h>

int cmd_csp_cmp_route_set(console_ctx_t *ctx, cmd_signature_t *reg)
{
    (void) ctx; // unused
    static struct {
        struct arg_int *node;
        struct arg_int *timeout;
        struct arg_int *addr;
        struct arg_int *via;
        struct arg_str *ifstr;
        struct arg_end *end;
    } args;

    if (reg) {
        args.node = arg_cspaddr1();
        args.timeout = arg_timeout0();
        args.addr = arg_int1(NULL, NULL, "<addr>", EXPENDABLE_STRING("target address"));
        args.ifstr = arg_str1(NULL, NULL, "<iface>", EXPENDABLE_STRING("interface name"));
        args.via = arg_int0("mv", "mac,via", "<mac>", EXPENDABLE_STRING("target MAC - route via address (default same as addr)"));
        args.end = arg_end(5);

        reg->argtable = &args;
        reg->help = EXPENDABLE_STRING("Set route in a CSP node via CMP");
        return 0;
    }

    int node = args.node->ival[0];
    int timeout = GET_ARG_TIMEOUT0(args.timeout);

    console_printf("Sending route_set to node %d timeout %d\n", node, timeout);

    int dest = args.addr->ival[0];
    int via = args.via->count ? args.via->ival[0] : dest;
    const char *iface = args.ifstr->sval[0];

    struct csp_cmp_message msg;
    msg.route_set.dest_node = (uint8_t) dest;
    msg.route_set.next_hop_via = (uint8_t) via;
    strncpy(msg.route_set.interface, iface, 10);
    console_printf("Dest_node: %u, next_hop_via: %u, interface %s\n",
                   msg.route_set.dest_node, msg.route_set.next_hop_via, msg.route_set.interface);

    int ret = csp_cmp_route_set(node, timeout, &msg);
    if (ret != CSP_ERR_NONE) {
        console_printf("CSP Error: %d\n", ret);
        return 1;
    } else {
        console_printf("Success\n");
    }

    return 0;
}
