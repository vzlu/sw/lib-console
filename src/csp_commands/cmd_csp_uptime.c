//
// Created by MightyPork on 2018/12/08.
//

#include "console/cmddef.h"
#include <malloc.h>

int cmd_csp_uptime(console_ctx_t *ctx, cmd_signature_t *reg)
{
    (void) ctx; // unused
    static struct {
        struct arg_int *node;
        struct arg_int *timeout;
        struct arg_end *end;
    } args;

    if (reg) {
        args.node = arg_cspaddr0();
        args.timeout = arg_timeout0();
        args.end = arg_end(2);

        reg->argtable = &args;
        reg->help = EXPENDABLE_STRING("Get uptime of a CSP node");
        return 0;
    }

    int node = GET_ARG_CSPADDR0(args.node);
    int timeout = GET_ARG_TIMEOUT0(args.timeout);

    uint32_t uptime = 0;
    int rv = csp_get_uptime(node, timeout, &uptime);
    if (rv == CSP_ERR_NONE) {
        console_printf("Uptime of node %u is %"PRIu32" s\n", node, uptime);
    } else {
        console_print("Network error\n");
    }

    return 0;
}
