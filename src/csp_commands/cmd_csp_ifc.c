//
// Show local CSP interface stats
//

#include "console/cmddef.h"

static void console_csp_iflist_print(void);

int cmd_csp_ifc(console_ctx_t *ctx, cmd_signature_t *reg)
{
    (void) ctx; // unused
    if (reg) {
        reg->help = EXPENDABLE_STRING("Show stats of local CSP interfaces");
        return 0;
    }

    console_csp_iflist_print();

    return 0;
}


static void console_csp_iflist_print(void) {
    csp_iface_t * i = csp_iflist_get();
    char txbuf[25];
    char rxbuf[25];

    while (i) {
        csp_bytesize(txbuf, sizeof(txbuf), i->txbytes);
        csp_bytesize(rxbuf, sizeof(rxbuf), i->rxbytes);
        console_printf("%-10s tx: %05"PRIu32" rx: %05"PRIu32" txe: %05"PRIu32" rxe: %05"PRIu32"\n"
               "           drop: %05"PRIu32" autherr: %05"PRIu32 " frame: %05"PRIu32"\n"
               "           txb: %"PRIu32" (%s) rxb: %"PRIu32" (%s) MTU: %u\n\n",
               i->name, i->tx, i->rx, i->tx_error, i->rx_error, i->drop,
               i->autherr, i->frame, i->txbytes, txbuf, i->rxbytes, rxbuf, i->mtu);
        i = i->next;
    }
}
