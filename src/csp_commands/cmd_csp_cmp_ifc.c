//
// Remote IFC via CSP CMP
//

#include "console/cmddef.h"
#include <csp/csp_cmp.h>
#include <csp/csp_endian.h>


int cmd_cmp_ifc(console_ctx_t *ctx, cmd_signature_t *reg)
{
    (void) ctx; // unused
    static struct {
        struct arg_int *node;
        struct arg_int *timeout;
        struct arg_str *ifstr;
        struct arg_end *end;
    } args;

    if (reg) {
        args.node = arg_cspaddr1();
        args.timeout = arg_timeout0();
        args.ifstr = arg_str1(NULL, NULL, "<iface>", EXPENDABLE_STRING("interface name"));
        args.end = arg_end(3);

        reg->argtable = &args;
        reg->help = EXPENDABLE_STRING("Read CSP node interface stats via CMP");
        return 0;
    }

    int node = args.node->ival[0];
    int timeout = GET_ARG_TIMEOUT0(args.timeout);

    const char *iface = args.ifstr->sval[0];

    console_printf("Sending cmp ifc[%s] to node %d timeout %d\n", iface, node, timeout);

    struct csp_cmp_message msg;
    strncpy(msg.if_stats.interface, iface, CSP_CMP_ROUTE_IFACE_LEN);

    int ret = csp_cmp_if_stats(node, timeout, &msg);
    if (ret != CSP_ERR_NONE) {
        console_printf("Error: %d\n", ret);
        return 1;
    }

    msg.if_stats.tx = csp_ntoh32(msg.if_stats.tx);
    msg.if_stats.rx = csp_ntoh32(msg.if_stats.rx);
    msg.if_stats.tx_error = csp_ntoh32(msg.if_stats.tx_error);
    msg.if_stats.rx_error = csp_ntoh32(msg.if_stats.rx_error);
    msg.if_stats.drop = csp_ntoh32(msg.if_stats.drop);
    msg.if_stats.autherr = csp_ntoh32(msg.if_stats.autherr);
    msg.if_stats.frame = csp_ntoh32(msg.if_stats.frame);
    msg.if_stats.txbytes = csp_ntoh32(msg.if_stats.txbytes);
    msg.if_stats.rxbytes = csp_ntoh32(msg.if_stats.rxbytes);
    msg.if_stats.irq = csp_ntoh32(msg.if_stats.irq);

    console_printf("%-5s   tx: %05"PRIu32" rx: %05"PRIu32" txe: %05"PRIu32" rxe: %05"PRIu32"\n"
                   "		drop: %05"PRIu32" autherr: %05"PRIu32" frame: %05"PRIu32"\n"
                   "		txb: %"PRIu32" rxb: %"PRIu32"\n\n",
                   msg.if_stats.interface, msg.if_stats.tx, msg.if_stats.rx, msg.if_stats.tx_error, msg.if_stats.rx_error, msg.if_stats.drop,
                   msg.if_stats.autherr, msg.if_stats.frame, msg.if_stats.txbytes, msg.if_stats.rxbytes);

    return 0;
}
