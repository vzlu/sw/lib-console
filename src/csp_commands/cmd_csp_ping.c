//
// Ping a CSP node
//

#include "console/cmddef.h"

int cmd_csp_ping(console_ctx_t *ctx, cmd_signature_t *reg)
{
    (void) ctx; // unused
    static struct {
        struct arg_int *node;
        struct arg_int *timeout;
        struct arg_int *size;
        struct arg_lit *opt_r;
        struct arg_lit *opt_x;
        struct arg_lit *opt_h;
        struct arg_lit *opt_c;
        struct arg_end *end;
    } args;

    if (reg) {
        args.node = arg_cspaddr1();
        args.timeout = arg_timeout0();
        args.size = arg_int0("s", "size", "<bytes>", EXPENDABLE_STRING("payload size (default 1)"));
        args.opt_r = arg_lit0("r", NULL, EXPENDABLE_STRING("use RDP"));
        args.opt_x = arg_lit0("x", NULL, EXPENDABLE_STRING("use XTEA"));
        args.opt_h = arg_lit0("H", NULL, EXPENDABLE_STRING("use HMAC"));
        args.opt_c = arg_lit0("c", NULL, EXPENDABLE_STRING("use CRC32"));
        args.end = arg_end(4);

        reg->argtable = &args;
        reg->help = EXPENDABLE_STRING("Ping a CSP node using the routing table");
        return 0;
    }

    int node = args.node->ival[0];
    int timeout = GET_ARG_TIMEOUT0(args.timeout);
    int size = GET_ARG_INT0(args.size, 1);

    uint32_t options = CSP_O_NONE;
    if (args.timeout->count) timeout = args.timeout->ival[0];
    if (args.size->count) size = args.size->ival[0];
    if (args.opt_r->count) options |= CSP_O_RDP;
    if (args.opt_x->count) options |= CSP_O_XTEA;
    if (args.opt_h->count) options |= CSP_O_HMAC;
    if (args.opt_c->count) options |= CSP_O_CRC32;

    console_printf("Ping node %d, timeout %u ms, size %u B: ", node, timeout, size);

    int time = csp_ping(node, timeout, size, options);

    if (time < 0) {
        console_printf("Timeout\n");
    } else if (time <= 1) {
        console_printf("Reply in <1 tick\n");
    } else {
        console_printf("Reply in %u ms\n", (unsigned int) time);
    }

    return 0;
}
