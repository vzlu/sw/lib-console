#include <stdio.h>
#include <console/console.h>
#include <console/utils.h>
#include <malloc.h>

#if CONSOLE_USE_MEMSTREAM

console_err_t console_filecap_init(console_filecap_t *cap) {
    cap->buf = NULL;
    cap->buf_size = 0;
    cap->file = open_memstream(&cap->buf, &cap->buf_size);
    if (!cap->file) {
        return CONSOLE_ERR_NO_MEM;
    }
    return CONSOLE_OK;
}

void console_filecap_end(console_filecap_t *cap) {
    // clean up
    if (cap->file) {
        fclose(cap->file);
        cap->file = NULL;
    }
    if (cap->buf) {
        free(cap->buf); // allocated by memstream
        cap->buf = NULL;
    }
}

void console_filecap_print_end(console_filecap_t *cap) {
    fflush(cap->file);
    fclose(cap->file);
    cap->file = NULL;

    if (cap->buf) {
        console_write(cap->buf, cap->buf_size);
        free(cap->buf); // allocated by memstream
        cap->buf = NULL;
    }
}

#endif
