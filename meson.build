project('console', 'c', version : '0.1')

console_src = files(
  'lib/argtable3/argtable3.c',
  'src/console.c',
  'src/console_filecap.c',
  'src/console_io.c',
  'src/console_linenoise.c',
  'src/console_prefix_match.c',
  'src/console_split_argv.c',
  'src/console_utils.c',
)

console_deps = []
console_inc = include_directories(
  'include',
  'lib/argtable3',
)

if get_option('use_termios') and not get_option('use_file_io_streams')
  error('Can\'t use TERMIOS without FILE_IO_STREAMS')
endif

if get_option('have_csp')
  csp = dependency('csp')
  console_deps += csp
endif

if get_option('use_csp_commands')
  console_src += files(
    'src/csp_commands/cmd_csp_buffree.c',
    'src/csp_commands/cmd_csp_cmp_clock.c',
    'src/csp_commands/cmd_csp_cmp_ifc.c',
    'src/csp_commands/cmd_csp_cmp_peek.c',
    'src/csp_commands/cmd_csp_cmp_poke.c',
    'src/csp_commands/cmd_csp_cmp_route_set.c',
    'src/csp_commands/cmd_csp_conn.c',
    'src/csp_commands/cmd_csp_debug.c',
    'src/csp_commands/cmd_csp_ident.c',
    'src/csp_commands/cmd_csp_ifc.c',
    'src/csp_commands/cmd_csp_memfree.c',
    'src/csp_commands/cmd_csp_ping.c',
    'src/csp_commands/cmd_csp_rdpopt.c',
    'src/csp_commands/cmd_csp_reboot.c',
    'src/csp_commands/cmd_csp_route_list.c',
    'src/csp_commands/cmd_csp_route_add.c',
    'src/csp_commands/cmd_csp_route_load.c',
    'src/csp_commands/cmd_csp_route_rm.c',
    'src/csp_commands/cmd_csp_route_save.c',
    'src/csp_commands/cmd_csp_route_set.c',
    'src/csp_commands/cmd_csp_rps.c',
    'src/csp_commands/cmd_csp_uptime.c',
    'src/csp_commands/cmd_bounce.c',
    'src/csp_commands/csp_commands.c',
    'src/csp_commands/x_csp_rt.c',
    'src/csp_commands/cmd_csp_pt.c',
  )
  if get_option('file_support')
    console_src += files(
      'src/csp_commands/cmd_csp_route_load.c',
      'src/csp_commands/cmd_csp_route_save.c',
    )
  endif
endif

core = get_option('mcu_core')
if core == 'stm32h7'
  stm32h7 = dependency('stm32h7-hal-core')
  console_deps += stm32h7
else
  warning('MCU core type unknown or unset: ' + core)
endif

console_lib = library(
  'console',
  console_src,
  dependencies : console_deps,
  include_directories : console_inc,
  install : true
)

console_dep = declare_dependency(
  include_directories : console_inc,
  link_with : console_lib
)
meson.override_dependency('console', console_dep)

pkgconfig = import('pkgconfig')
pkgconfig.generate(console_lib)

console_config = configuration_data()
console_config.set('CONSOLE_LINE_BUF_LEN', get_option('line_buf_len'))
console_config.set('CONSOLE_MAX_NUM_ARGS', get_option('max_num_args'))
console_config.set('CONSOLE_PROMPT_MAX_LEN', get_option('prompt_max_len'))
console_config.set('CONSOLE_HISTORY_LEN', get_option('history_len'))
console_config.set('CONSOLE_CSP_DEF_TIMEOUT_MS', get_option('csp_def_timeout_ms'))
console_config.set10('CONSOLE_FILE_SUPPORT', get_option('file_support'))
console_config.set10('CONSOLE_USE_TERMIOS', get_option('use_termios'))
console_config.set10('CONSOLE_USE_MEMSTREAM', get_option('use_memstream'))
console_config.set10('CONSOLE_USE_CSP_COMMANDS', get_option('use_csp_commands'))
console_config.set10('CONSOLE_USE_FILE_IO_STREAMS', get_option('use_file_io_streams'))
console_config.set10('CONSOLE_HAVE_CSP', get_option('have_csp'))
console_config.set10('CONSOLE_TESTING_ALLOC_FUNCS', get_option('testing_alloc_funcs'))

subdir('include/console')

install_subdir('include/console', install_dir : get_option('includedir'),
  exclude_files : ['meson.build', 'config.h.in'])
install_headers('lib/argtable3/argtable3.h')
