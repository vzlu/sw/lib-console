//
// Header with useful defines and common includes
// to use when defining console commands.
//
// This file aims to concentrate the most common includes
// and utility macros to make command definitions easier to write.
//
// Created by MightyPork on 2020/03/11.
//

#ifndef LIBCONSOLE_CMDDEF_H
#define LIBCONSOLE_CMDDEF_H

#include <stdint.h>
#include <argtable3.h>
#include "console/console.h"

#if CONSOLE_HAVE_CSP
#include <csp/csp.h>
#endif

#include "console/utils.h"

#endif //LIBCONSOLE_CMDDEF_H
