/**
 * Console IO functions.
 *
 * This header is included internally by console.h
 * 
 * Created on 2020/04/09.
 */

#ifndef LIBCONSOLE_IO_H
#define LIBCONSOLE_IO_H

#ifndef LIBCONSOLE_H
#error Include console.h!
#endif

#include <stdarg.h>

// ------ If the FILE based IO streams option is OFF, these are extern -------

/**
 * Write to console context.
 *
 * In command context, the more convenient "console_write", "console_print", "console_println"
 * and "console_printf" functions can be used instead.
 *
 * This function is a Linenoise write callback.
 *
 * Return number of characters written, -1 on error.
 */
extern int console_write_ctx(console_ctx_t *ctx, const char *text, size_t len);

/**
 * Read from console context's input stream.
 *
 * In command context, the more convenient "console_read" function and the
 * "console_can_read" and "console_have_stdin" helper functions can be used instead.
 *
 * This is also a Linenoise read callback.
 *
 * Return number of characters read, -1 on error
 */
extern int console_read_ctx(console_ctx_t *ctx, char *dest, size_t count);

/**
 * Check if console input stream has bytes ready.
 *
 * @return number of queued bytes, 0 if none, -1 on error.
 */
extern int console_can_read_ctx(console_ctx_t *ctx);

/**
 * Test if console context is not NULL and has stdin stream available
 *
 * @return have stdin
 */
extern bool console_have_stdin_ctx(console_ctx_t *ctx);


// ----- end extern interface -----

/**
 * Print zero-terminated string to to console output
 *
 * @param text - characters to write
 * @return number of characters written, or -1 on error
 */
int console_print_ctx(console_ctx_t *ctx, const char *text);

/**
 * Print zero-terminated string to console output, followed by a newline
 *
 * @param text - characters to write
 * @return number of characters written, or -1 on error
 */
ssize_t console_println_ctx(console_ctx_t *ctx, const char *text);

/**
 * Console printf
 *
 * @param ctx - console context
 * @param color - color to use, COLOR_RESET = default
 * @param format
 * @param ...
 * @return bytes written, or -1 on error
 */
ssize_t console_printf_ctx(console_ctx_t *ctx, console_color_t color, const char *format, ...) __attribute__((format(printf,3,4)));

/**
 * Console vprintf
 *
 * @param ctx - console context
 * @param color - color to use, COLOR_RESET = default
 * @param format - format string
 * @param args - varargs passed as a va_list
 * @return bytes written, or -1 on error
 */
ssize_t console_vprintf_ctx(console_ctx_t *ctx, console_color_t color, const char *format, va_list args);

/**
 * Write to console output
 *
 * @attention Can only be used within a console command context
 *
 * @param text - characters to write
 * @param len - text length
 * @return number of characters written, or -1 on error
 */
ssize_t console_write(const char *text, size_t len);


// -------------------- Convenience functions -------------------------

/**
 * Test if we are in the console command context.
 *
 * @return in command context
 */
static inline bool console_context_available(void) {
    return console_active_ctx != NULL;
}

/**
 * Test if we are in the console command context AND the console context has an input stream
 * (input stream may be used in interactive commands)
 *
 * @return have stdin
 */
bool console_have_stdin(void);

/**
 * Console printf. Defined as a macro to pass variadic arguments
 *
 * @attention Can only be used within a console command context
 *
 * @param format
 * @param ...
 * @return bytes written, or -1 on error
 */
#define console_printf(format, ...) console_printf_ctx(console_active_ctx, COLOR_RESET, format, ##__VA_ARGS__)

/**
 * Console printf with colors. Defined as a macro to pass variadic arguments
 *
 * @attention Can only be used within a console command context
 *
 * @param color - from console_colors_t enum
 * @param format
 * @param ...
 * @return bytes written, or -1 on error
 */
#define console_color_printf(color, format, ...) console_printf_ctx(console_active_ctx, color, format, ##__VA_ARGS__)

/**
 * Read from console input
 *
 * @attention Can only be used within a console command context
 *
 * @param dest - destination buffer
 * @param count - how many characters to read
 * @return number of characters read, or -1 on error
 */
ssize_t console_read(char *dest, size_t count);

/**
 * Check if console input stream has bytes ready.
 *
 * @attention Can only be used within a console command context
 *
 * @return number of queued bytes, 0 if none, -1 on error.
 */
int console_can_read(void);

/**
 * Print zero-terminated string to to console output
 *
 * @attention Can only be used within a console command context
 *
 * @param text - characters to write
 * @return number of characters written, or -1 on error
 */
static inline int console_print(const char *text) {
    return console_print_ctx(console_active_ctx, text);
}

/**
 * Print zero-terminated string to console output, followed by a newline
 *
 * @attention Can only be used within a console command context
 *
 * @param text - characters to write
 * @return number of characters written, or -1 on error
 */
ssize_t console_println(const char *text);


#endif //LIBCONSOLE_IO_H
